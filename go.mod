module vrchat-osc-go

go 1.22.3

require (
	github.com/biter777/processex v0.0.0-20210102170504-01bb369eda71 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/hypebeast/go-osc v0.0.0-20220308234300-cec5a8a1e5f5 // indirect
	github.com/mitchellh/go-ps v1.0.0 // indirect
	github.com/zmb3/spotify/v2 v2.4.2 // indirect
	golang.org/x/net v0.23.0 // indirect
	golang.org/x/oauth2 v0.0.0-20210810183815-faf39c7919d5 // indirect
	golang.org/x/sys v0.20.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
