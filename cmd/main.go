package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
	oscclient "vrchat-osc-go/cmd/osc-client"
	spotifyauthpack "vrchat-osc-go/cmd/web"

	"github.com/zmb3/spotify/v2"
	"gopkg.in/ini.v1"
)

var configFile string = "config.ini"
var iniConfig *ini.File

const TOKEN_ENDPOINT = `https://accounts.spotify.com/api/token`
const NOW_PLAYING_ENDPOINT = `https://api.spotify.com/v1/me/player/currently-playing`

func main() {
	// check config file
	if _, err := os.Stat(configFile); err != nil {
		fmt.Println("Config file not found. Creating new config file..")
		if _, cerr := os.Create(configFile); cerr != nil {
			fmt.Printf("Failed to create config file %s\nCreate file named %s and try to run again\n", cerr, configFile)
			os.Stdin.Read(make([]byte, 1))
			os.Exit(1)
		}
	}

	cfgData, err := os.ReadFile(configFile)
	if err != nil {
		fmt.Printf("Failed to read config file %s\n", err)
	}
	if len(string(cfgData)) < 1 {
		fmt.Println("Config file is empty loading template config file")
		res, geterr := http.Get("https://gitlab.com/unwrapshell/vrcspotify-ocs-go/-/raw/main/config.ini?ref_type=heads")
		if geterr != nil {
			fmt.Printf("Failed to get config template %s\nGo to https://gitlab.com/unwrapshell/vrcspotify-ocs-go/-/raw/main/config.ini to download template\n", geterr)
			os.Stdin.Read(make([]byte, 1))
			os.Exit(1)
		}
		body, err := io.ReadAll(res.Body)
		if err != nil {
			fmt.Printf("An error occure %s\n", err)
		}
		err = os.WriteFile(configFile, []byte(string(body)), 0666)
		if err != nil {
			fmt.Printf("Failed to write config file %s", err)
			os.Stdin.Read(make([]byte, 1))
			os.Exit(1)

		}
		fmt.Println("Config file loaded")
		fmt.Println("Please edit config file and run again")
		os.Stdin.Read(make([]byte, 1))
		os.Exit(1)
	}

	// check if VRChat is running by executing powershell and run Get-Process
	fmt.Println("Waiting for VRChat to run..")
	for {
		out, _ := exec.Command("powershell", "Get-Process VRChat").Output()
		if len(out) > 0 {
			fmt.Println("VRChat is running")
			break
		}
		time.Sleep(1 * time.Second)
	}

	// load iniConfig
	cfg, err := ini.Load(configFile)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Stdin.Read(make([]byte, 1))
		os.Exit(1)
	}
	iniConfig = cfg
	ip := iniConfig.Section("").Key("localHost").String()
	port, err := strconv.Atoi(iniConfig.Section("").Key("localPort").String())
	if err != nil {
		fmt.Printf("An error occure %s\n", err)
		os.Stdin.Read(make([]byte, 1))
		os.Exit(1)
	}
	webport, err := strconv.Atoi(iniConfig.Section("").Key("webserverPort").String())
	if err != nil {
		fmt.Printf("An error occure %s\n", err)
		os.Stdin.Read(make([]byte, 1))
		os.Exit(1)
	}
	updateTime, err := strconv.Atoi(iniConfig.Section("").Key("updateTime").String())
	if err != nil {
		fmt.Printf("An error occure %s\n", err)
		os.Stdin.Read(make([]byte, 1))
		os.Exit(1)
	}

	// start spotify auth server
	client := spotifyauthpack.SpotifyAuth(webport, iniConfig.Section("").Key("client_id").String(), iniConfig.Section("").Key("client_secret").String())
	// connect to server
	oscclient.Connect(ip, port)
	Update(int64(updateTime), client)
}

func Update(updateTime int64, client *spotify.Client) {
	for {
		player, err := client.PlayerCurrentlyPlaying(context.Background())
		var progress = func() string {
			if player.Item == nil {
				return "[----------]"
			}
			var currentProgress = player.Progress
			var duration = player.Item.Duration
			var progress float32 = float32(currentProgress) / float32(duration)
			return fmt.Sprintf("[%s]", strings.Repeat("=", int(progress*10))+strings.Repeat("-", 10-int(progress*10)))
		}
		if err != nil {
			fmt.Println("Failed to get currently playing song")
		}
		if player.Item != nil {
			progressValue := progress() // Call the progress function
			oscclient.SendMessage("/chatbox/input", []any{
				fmt.Sprintf("%s - %s\n%s", player.Item.Name, player.Item.Artists[0].Name, progressValue), // Use the progressValue variable
				true,
				false,
			})
		}

		time.Sleep(time.Duration(updateTime) * time.Millisecond)
	}
}
