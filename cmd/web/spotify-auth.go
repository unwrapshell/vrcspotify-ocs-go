package spotifyauthpack

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os/exec"
	"runtime"

	"github.com/zmb3/spotify/v2"
	spotifyauth "github.com/zmb3/spotify/v2/auth"
)

var (
	auth  *spotifyauth.Authenticator
	ch    = make(chan *spotify.Client)
	state = "abc123"
)

func SpotifyAuth(webport int, spotifyID string, spotifySecert string) *spotify.Client {
	fmt.Println("Starting Spotify Auth Server")
	auth = spotifyauth.New(spotifyauth.WithRedirectURL(fmt.Sprintf("http://localhost:%d/callback", webport)), spotifyauth.WithScopes(spotifyauth.ScopeUserReadPrivate, spotifyauth.ScopeUserReadCurrentlyPlaying), spotifyauth.WithClientID(spotifyID), spotifyauth.WithClientSecret(spotifySecert))

	http.HandleFunc("/callback", completeAuth)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Println("Got request for:", r.URL.String())
	})
	go func() {
		err := http.ListenAndServe(fmt.Sprintf(":%d", webport), nil)
		if err != nil {
			log.Fatal(err)
		}
	}()
	url := auth.AuthURL(state)
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "powershell"
		args = []string{"Start-Process", fmt.Sprintf("'%s'", url)}
	case "darwin":
		cmd = "open"
		args = []string{url}
	case "linux":
		cmd = "xdg-open"
		args = []string{url}
	default:
		fmt.Println("Unsupported platform")
	}
	if err := exec.Command(cmd, args...).Start(); err != nil {
		fmt.Printf("Failed to open browser %s\n", err)
		fmt.Println("Please navigate here:", url)

	}
	client := <-ch
	user, err := client.CurrentUser(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("You are logged in as:", user.ID)
	// get user current playing
	return client
}

func completeAuth(w http.ResponseWriter, r *http.Request) {
	tok, err := auth.Token(r.Context(), state, r)
	if err != nil {
		http.Error(w, "Couldn't get token", http.StatusForbidden)
		log.Fatal(err)
	}
	if st := r.FormValue("state"); st != state {
		http.NotFound(w, r)
		log.Fatalf("State mismatch: %s != %s\n", st, state)
	}
	client := spotify.New(auth.Client(r.Context(), tok))
	fmt.Fprintf(w, "Login Completed!")
	ch <- client
}
