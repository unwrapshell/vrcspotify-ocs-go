package oscclient

import (
	"fmt"

	"github.com/hypebeast/go-osc/osc"
)

var client = osc.NewClient("localhost", 9000)

func GetClient() *osc.Client {
	return client
}

func Connect(targetip string, port int) {
	fmt.Println("Connnecting to OSC Server")
	client = osc.NewClient(targetip, port)
}

func SendMessage(addr string, message []any) {
	chatbox := osc.NewMessage(addr)
	for _, s := range message {
		chatbox.Append(s)
	}
	client.Send(chatbox)
}
