# VRChat Go osc
This project provide with a simple binding for connect to VRChat OSC using go language. My plan is to make spotify current playing work using pure go so far the function of the current state of this project is

```ini
[Currently work]
- Display system time
- Display Current Playing Spotify Track (Yah!)

[Todo]
- Custom text format so that it's could be customize like PC status and Heart Rate
- Graphic User Interface (? maybe a lightweight one like native to platform or use webview i don't know feel free to suggest)
```

## How to build
This section will guide you through building and running your Go project for interacting with VRChat Go OSC.

### Prerequisites:
- Go 1.22.3 and above 


### Command
Download required package
```powershell
go mod download
```
Build an application
```powershell
go build .\cmd\main.go
```



## Installation
This section will guide you through installing the VRChat Go OSC application on your system.

### Prerequisites:
- Windows 10 and above
- Linux (didn't test yet)
- MacOS (didn't test yet)

### Setup
1. Download the latest release from the [release page](https://gitlab.com/unwrapshell/vrcspotify-ocs-go/-/releases)

2. Launch the application. this will initialize the configuration file in the same directory as the application.

3. Open [spotify developer dashboard](https://developer.spotify.com/dashboard) and create a new application. Copy the client id and client secret to the configuration file [How?](https://developer.spotify.com/documentation/web-api/concepts/apps).
> Note Redirection URI should be `http://localhost:{port}/callback` where port is the port that you set in the configuration file Default is 9001

4. Launch the VRChat and go to the world that you want to use the application. Open the radial menu and click on the `Options` > `OSC` > `Enable` and `Set Port` to the port that you set in the configuration file Default is 9000

5. Launch the application. the application will prompt you to login to your Spotify account. After you login the application will start displaying the current playing track.

6. Enjoy!

